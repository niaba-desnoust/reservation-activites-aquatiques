# Architecture du système de réservation

La méthodologie utilisée pour modéliser et documenter l’architecture logicielle du système de réservation est le [modèle C4](https://c4model.com/) (**C**ontext, **C**ontainer, **C**omponent et **C**ode).

Cette approche permet de représenter le système selon différents niveaux de détail. Le contexte du système est présenté en premier, puis sa structure interne. 

Les sections suivantes montrent le système selon trois niveaux, classés du plus global au plus précis&nbsp;:

- Le niveau 1 : le contexte du système
- Le niveau 2 : les conteneurs du système
- Le niveau 3 : les composants des conteneurs du système

> **Attention**&nbsp;: comme vous pouvez le constater, cette méthodologie se base sur plusieurs abstractions (Contexte, Conteneurs, Composants) qui ont un sens différent de celui qu'on pourrait leur attribuer en temps normal. Ainsi, le terme **Conteneur** n'est pas relatif à la technologie Docker, tout comme les **Composants** ne sont pas relatifs à un framework Javascript (Angular, React, etc.).
> Vous pouvez vous référer à la [documentation du modèle C4](https://c4model.com/) pour en savoir plus sur la signification de ces abstractions.

## Le contexte du système

Ce diagramme montre le contexte du système, c'est-à-dire les interactions entre le système et des acteurs/systèmes externes.

![System context diagram pour le système de réservation](System%20context%20diagram%20pour%20le%20système%20de%20réservation.drawio.png)

## Les conteneurs du système

Ce diagramme montre à haut niveau l'architecture du système et la façon dont les responsabilités sont réparties. Il montre également les principaux choix technologiques et la manière dont les conteneurs communiquent entre eux.

![Container Diagram pour le système de réservation](Container%20Diagram%20pour%20le%20système%20de%20réservation.drawio.png)

## Diagramme des composants du back-end du système

Ce diagramme montre comment le back-end est organisé en "composants", ce que sont chacun de ces composants, leurs responsabilités et les détails de la technologie/de la mise en œuvre.

![Component Diagram pour le back-end du système de réservation](Component%20Diagram%20pour%20le%20back-end%20du%20système%20de%20réservation.drawio.png)

## Diagramme des composants du front-office du système

Ce diagramme montre comment le front-office est organisé en "composants", ce que sont chacun de ces composants, leurs responsabilités et les détails de la technologie/de la mise en œuvre.

![Component Diagram pour le front-office du système de réservation](Component%20Diagram%20pour%20le%20front-office%20du%20système%20de%20réservation.drawio.png)
