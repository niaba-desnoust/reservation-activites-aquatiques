# Projet de réservation d'activités aquatiques

Mono-repository d'une application web permettant la réservation en ligne d'activités aquatiques. Cette application est conçue et développée bénévolement pour répondre au [besoin de la communauté d'agglomération Rochefort Océan](https://mon.incubateur.anct.gouv.fr/processes/transformation-numerique/f/5/proposals/293). 

## Résumé du besoin
Dans le contexte d'amélioration du service aux usagers de la piscine municipale, la communauté d'agglomération souhaiterait proposer une solution afin que les citoyens, associations, scolaires... puissent réserver en ligne des activités aquatiques et leur permettre ainsi d'avoir une idée quant à la capacité d'occupation des lieux. La gestion au quotidien des agents municipaux s'en trouverait grandement améliorée.

## Auteurs

Ce projet est initié et développé par deux développeurs Full-stack:
- [Victoria Niaba](https://github.com/VictoriaNiaba)
- [Nicolas Desnoust](https://github.com/NicolasDesnoust)

## Structure du projet
Le projet est principalement développé à l'aide des Frameworks Spring (Java) et Angular (Typescript).

Il est composé de quatre parties :
- Une [base de données](database/README.md) relationnelle PostgreSQL
- Un [back-end](back-end/README.md), côté serveur et dont les services sont utilisables via une API REST.
- Un [front-office](front-end/projects/front-office/README.md), côté client et développé sous forme d'une Single Page Application (SPA). Il permet à des particuliers, écoles ou associations de réserver des activités aquatiques dans la communauté d'agglomération de Rochefort Océan.
- Un [back-office](front-end/projects/back-office/README.md), côté client et également développé sous forme d'une SPA. Il permet au personnel de la communauté d'agglomération de gérer les réservations.

## Lancer le projet en développement

Les composants sont tous configurés pour être exécutés dans des conteneurs, à l'aide de [Docker](https://www.docker.com/resources/what-container). De ce fait, l'application peut être exécutée dans un environnement proche de celui de production, rapidement sur une nouvelle machine et sans crainte d'incompatibilités entre systèmes d'exploitation.

### Prérequis

- [Docker](https://docs.docker.com/get-docker/) version 18.06.0+
- [Docker Compose](https://docs.docker.com/compose/install/) version 3.7+ 

### Lancer le projet

Une fois Docker et Docker Compose installés, lancez un terminal dans le dossier racine du projet et exécutez la commande `docker-compose up`. Cette commande lance l'intégralité des composants listés précédemment. Pour lancer les composants en arrière-plan, ajoutez l'option `-d` à la commande.

À ce stade, le back-end, front-office, back-office sont respectivement accessibles via un navigateur aux URLs suivantes : `localhost:8080`, `localhost:4200` et `localhost:4201`.
Le front-office et le back-office se relanceront automatiquement si vous changez n'importe quel fichier source. En revanche, pour que le back-end se relance également automatiquement, une étape supplémentaire est nécessaire et est documentée dans son [README](back-end/README.md) respectif.

### Consulter les logs des composants

Si les conteneurs ont été créés détachés du terminal, les logs des applications ne sont pas directement visibles dans le terminal. Pour les consulter, utilisez la commande:
```bash
docker logs --tail 100 -f <nom-du-conteneur-docker>
```
où `<nom-du-conteneur-docker>` correspond au nom du conteneur que la commande à créé pour chaque application. Les noms des conteneurs sont consultables à l'aide de la commande:
```bash
docker container ls
```
> Les noms des conteneurs devraient ressembler à `reservation-activites-aquatiques_front-office_1`