# Front-end du projet de réservation d'activités aquatiques

Ce dossier a été généré avec [le CLI d'Angular](https://github.com/angular/angular-cli) en version 10.2.0.

Il s'agit d'un espace de travail angular multi-projet, adapté au fait que tous les composants de l'application soient groupés dans une seule repository. Les applications angular sont présentes dans le dossier `/projects` et se partagent une configuration globale.

L'espace de travail contient deux applications:
- Le [Front-office](projects/front-office/README.md), qui permet à des particuliers, écoles ou associations de réserver des activités aquatiques dans la communauté d'agglomération de Rochefort Océan.
- Le [Back-office](projects/back-office/README.md), qui permet au personnel de la communauté d'agglomération de gérer les réservations.

## Lancer les applications sur un serveur de développement

En développement, les composants de l'application peuvent être exécutés ensemble à l'aide de [Docker Compose](https://docs.docker.com/compose/). Les instructions à suivre sont documentées dans le [README du projet](../README.md).

Si vous voulez néanmoins lancer une application Angular seule, vous pouvez le faire avec la commande `npm run start:<nom-de-l'application>`. Naviguez vers `http://localhost:4200/`. L'application se relancera automatiquement si vous changez n'importe quel fichier source.
Attention: les applications Angular dépendent fortement du back-end. La plupart des fonctionnalités ne seront pas disponibles si vous ne le lancez pas également.

## Compiler les applications

Exécuter `ng build --app=<nom-de-l'application>` pour compiler une application. Le résultat de la compilation sera stocké dans le dossier `dist/`. Utilisez l'option `--prod` pour compiler avec la configuration de production.
