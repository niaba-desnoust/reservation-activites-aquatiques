import { Component, Input } from '@angular/core';
import { ErrorMessage } from '../../model/error-message';

@Component({
  selector: 'raa-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.scss'],
})
export class ErrorMessageComponent {
  @Input() errorMessage: ErrorMessage | undefined;
}
