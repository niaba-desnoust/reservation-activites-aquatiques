import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ErrorMessage } from '../../shared/model/error-message';

@Component({
  selector: 'raa-not-found-page',
  template: `
    <raa-error-message [errorMessage]="errorMessage"></raa-error-message>
  `,
  styles: [
    `
      :host {
        display: flex;
        padding-top: 8rem;
      }
    `,
  ],
})
export class NotFoundPageComponent {
  errorMessage: ErrorMessage = {
    image: {
      url: 'assets/not-found.svg',
      realWidth: 300,
      realHeight: 163.113,
      desiredWidth: 300,
    },
    title: "La page demandée n'a pas été trouvée",
    message:
      "Vous pouvez retourner à la page d'accueil en cliquant sur le bouton ci-dessous.",
    button: {
      label: "Aller à l'accueil",
      action: () => {
        this.navigateToHomePage();
      },
    },
  };

  constructor(private router: Router) {}

  navigateToHomePage(): void {
    this.router.navigate(['/']);
  }
}
