import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ErrorMessage } from '../../shared/model/error-message';

@Component({
  selector: 'raa-internal-server-error-page',
  template: `
    <raa-error-message [errorMessage]="errorMessage"></raa-error-message>
  `,
  styles: [
    `
      :host {
        display: flex;
        padding-top: 8rem;
      }
    `,
  ],
})
export class InternalServerErrorPageComponent {
  errorMessage: ErrorMessage = {
    image: {
      url: 'assets/server-down.svg',
      realWidth: 300,
      realHeight: 187.3,
      desiredWidth: 300,
    },
    title: 'Le serveur a rencontré une erreur',
    message:
      "Vous pouvez retourner à la page d'accueil ou revenir plus tard si le problème persiste.",
    button: {
      label: "Aller à l'accueil",
      action: () => {
        this.navigateToHomePage();
      },
    },
  };

  constructor(private router: Router) {}

  navigateToHomePage = () => {
    this.router.navigate(['/']);
  };
}
