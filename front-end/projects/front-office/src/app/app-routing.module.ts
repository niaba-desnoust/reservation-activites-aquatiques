import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InternalServerErrorPageComponent } from './core/components/internal-server-error-page.component';
import { NotFoundPageComponent } from './core/components/not-found-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'piscines',
    pathMatch: 'full',
  },
  {
    path: 'piscines',
    loadChildren: () =>
      import('./swimming-pool/swimming-pool.module').then(
        (m) => m.SwimmingPoolModule
      ),
  },
  {
    path: 'erreur-interne-serveur',
    component: InternalServerErrorPageComponent,
  },
  {
    path: '**',
    component: NotFoundPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
