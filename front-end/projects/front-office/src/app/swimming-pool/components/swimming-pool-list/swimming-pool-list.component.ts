import { DOCUMENT } from '@angular/common';
import { Component, Inject, Input } from '@angular/core';
import { ErrorMessage } from '../../../shared/model/error-message';
import { SwimmingPool } from '../../model/swimming-pool';

@Component({
  selector: 'raa-swimming-pool-list',
  templateUrl: './swimming-pool-list.component.html',
})
export class SwimmingPoolListComponent {
  @Input() swimmingPools: SwimmingPool[] = [];

  errorMessage: ErrorMessage = {
    image: {
      url: 'assets/no-data.svg',
      realWidth: 150,
      realHeight: 146.417,
      desiredWidth: 150,
    },
    title: "Aucune piscine n'a été trouvée",
    message:
      'Vous pouvez actualiser la page ou revenir plus tard si le problème persiste.',
    button: {
      label: 'Actualiser la page',
      action: () => {
        this.refreshCurrentPage();
      },
    },
  };

  constructor(@Inject(DOCUMENT) private document: Document) {}

  refreshCurrentPage(): void {
    this.document.defaultView?.location.reload();
  }
}
