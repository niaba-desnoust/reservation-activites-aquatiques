import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { SwimmingPool } from '../../model/swimming-pool';

@Component({
  selector: 'raa-swimming-pool-page',
  templateUrl: './swimming-pool-page.component.html',
  styleUrls: ['./swimming-pool-page.component.scss'],
})
export class SwimmingPoolPageComponent implements OnInit {
  swimmingPools: SwimmingPool[] = [];

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.route.data.subscribe(
      (data: Data) => (this.swimmingPools = data.swimmingPools)
    );
  }
}
