import { Component, Input } from '@angular/core';
import { SwimmingPool } from '../../model/swimming-pool';

@Component({
  selector: 'raa-swimming-pool-card',
  templateUrl: './swimming-pool-card.component.html',
  styleUrls: ['./swimming-pool-card.component.scss'],
})
export class SwimmingPoolCardComponent {
  @Input() swimmingPool: SwimmingPool | undefined;
}
