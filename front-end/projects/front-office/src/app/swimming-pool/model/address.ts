export interface Address {
  city: string;
  postalCode: string;
  line1: string;
  line2?: string;
}
