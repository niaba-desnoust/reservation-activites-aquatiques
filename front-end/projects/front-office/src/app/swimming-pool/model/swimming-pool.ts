import { Address } from './address';

export interface SwimmingPool {
  id: number;
  name: string;
  website?: string;
  address: Address;
}
