import { NgModule } from '@angular/core';

import { SwimmingPoolRoutingModule } from './swimming-pool-routing.module';
import { SharedModule } from '../shared/shared.module';
import { SwimmingPoolListComponent } from './components/swimming-pool-list/swimming-pool-list.component';
import { SwimmingPoolPageComponent } from './components/swimming-pool-page/swimming-pool-page.component';
import { SwimmingPoolCardComponent } from './components/swimming-pool-card/swimming-pool-card.component';

@NgModule({
  declarations: [
    SwimmingPoolListComponent,
    SwimmingPoolPageComponent,
    SwimmingPoolCardComponent,
  ],
  imports: [SwimmingPoolRoutingModule, SharedModule],
})
export class SwimmingPoolModule {}
