import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SwimmingPoolResolver } from './services/swimming-pool.resolver';

import { SwimmingPoolPageComponent } from './components/swimming-pool-page/swimming-pool-page.component';

const routes: Routes = [
  {
    path: '',
    component: SwimmingPoolPageComponent,
    resolve: {
      swimmingPools: SwimmingPoolResolver,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SwimmingPoolRoutingModule {}
