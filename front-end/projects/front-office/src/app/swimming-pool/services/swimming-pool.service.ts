import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'projects/front-office/src/environments/environment';
import { Observable } from 'rxjs';
import { SwimmingPool } from '../model/swimming-pool';


@Injectable({
  providedIn: 'root',
})
export class SwimmingPoolService {
  private url = `${environment.backendUrl}/api/swimming-pools`;

  constructor(private httpClient: HttpClient) {}

  findAllSwimmingPools(): Observable<SwimmingPool[]> {
    return this.httpClient.get<SwimmingPool[]>(this.url);
  }
}
