import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';

import { SwimmingPool } from '../model/swimming-pool';
import { SwimmingPoolService } from './swimming-pool.service';

@Injectable({
  providedIn: 'root',
})
export class SwimmingPoolResolver implements Resolve<SwimmingPool[]> {
  constructor(private swimmingPoolService: SwimmingPoolService) {}

  resolve(): Observable<SwimmingPool[]> {
    return this.swimmingPoolService.findAllSwimmingPools();
  }
}
