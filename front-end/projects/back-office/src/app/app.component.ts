import { Component } from '@angular/core';

@Component({
  selector: 'raa-root',
  template: ` <router-outlet></router-outlet> `,
})
export class AppComponent {
  title = 'back-office';
}
