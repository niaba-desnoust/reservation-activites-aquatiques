# Back-end du projet de réservation d'activités aquatiques


## Consulter la documentation de l'API REST

Une fois l'application lancée, vous pouvez consulter la documentation de l'API REST à deux URLs différentes :

- [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html), sous forme de documentation interactive.
- [http://localhost:8080/v3/api-docs/](http://localhost:8080/v3/api-docs/), sous forme de document json.