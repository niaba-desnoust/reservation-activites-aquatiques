package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import static com.rochefortocean.reservationactivitesaquatiques.swimmingpool.AddressDTO.anAddressDTO;
import static com.rochefortocean.reservationactivitesaquatiques.swimmingpool.AddressDataBuilder.anAddress;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
class AddressMapperUnitTest {

    AddressMapperImpl addressMapper = new AddressMapperImpl();

    @Test
    void givenAnAddress_whenMapToDTO_thenSuccess() {
        // ------------------------------ Given ------------------------------//
        Address address = anAddress().build();

        AddressDTO expected = anAddressDTO()
                .withCity(address.getCity())
                .withPostalCode(address.getPostalCode())
                .withLine1(address.getLine1())
                .withLine2(address.getLine2())
                .build();

        // ------------------------------ When ------------------------------//
        AddressDTO actual = addressMapper.toDto(address);

        // ------------------------------ Then ------------------------------//
        assertThat(actual).isEqualTo(expected);
    }
}
