package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import java.util.Collections;
import java.util.List;

public class AddressDataBuilder {

    public static Address.AddressBuilder anAddress() {
        return Address.anAddress()
                .withCity("Rochefort")
                .withPostalCode("17300")
                .withLine1("Rue Charles Maher");
    }

    public static List<Address> anAddressList() {
        return Collections.singletonList(anAddress().build());
    }

    public static AddressDTO.AddressDTOBuilder anAddressDTO() {
        return AddressDTO.anAddressDTO()
                .withCity("Rochefort")
                .withPostalCode("17300")
                .withLine1("Rue Charles Maher");
    }

    public static List<AddressDTO> anAddressDTOList() {
        return Collections.singletonList(anAddressDTO().build());
    }
}
