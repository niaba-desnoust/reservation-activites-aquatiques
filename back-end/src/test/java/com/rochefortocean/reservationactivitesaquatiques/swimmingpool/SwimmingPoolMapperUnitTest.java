package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import static com.rochefortocean.reservationactivitesaquatiques.swimmingpool.SwimmingPoolDTO.aSwimmingPoolDTO;
import static com.rochefortocean.reservationactivitesaquatiques.swimmingpool.SwimmingPoolDataBuilder.aSwimmingPool;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class SwimmingPoolMapperUnitTest {

    private @Mock AddressMapperImpl addressMapper;
    private @InjectMocks SwimmingPoolMapperImpl swimmingPoolMapper;

    @Test
    void givenASwimmingPool_whenMapToDTO_thenSuccess() {
        // ------------------------------ Given ------------------------------//
        SwimmingPool swimmingPool = aSwimmingPool().withAddress(null).build();

        SwimmingPoolDTO expected = aSwimmingPoolDTO()
                .withId(swimmingPool.getId())
                .withName(swimmingPool.getName())
                .withWebsite(swimmingPool.getWebsite())
                .build();

        // ------------------------------ When ------------------------------//
        SwimmingPoolDTO actual = swimmingPoolMapper.toDto(swimmingPool);

        // ------------------------------ Then ------------------------------//
        assertThat(actual).isEqualTo(expected);
        verify(addressMapper, times(1)).toDto(any());
    }
}
