package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import static com.rochefortocean.reservationactivitesaquatiques.swimmingpool.SwimmingPoolDataBuilder.aSwimmingPool;
import static io.restassured.RestAssured.get;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;

import io.restassured.http.ContentType;
import io.restassured.response.Response;

@ActiveProfiles("test")
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class SwimmingPoolControllerIntegrationTest {

    @MockBean
    private SwimmingPoolRepository repository;

    @LocalServerPort
    private int port;

    private String baseUri;

    @PostConstruct
    void init() {
        baseUri = "http://localhost:" + port + "/api/swimming-pools";
    }

    @Test
    void whenMakingGetRequestToSwimmingPoolsEndpoint_thenReturnAllSwimmingPools() {
        // ------------------------------ Given ------------------------------//
        SwimmingPool expectedSwimmingPool = aSwimmingPool().build();
        when(repository.findAll()).thenReturn(Collections.singletonList(expectedSwimmingPool));

        // ------------------------------ When ------------------------------//
        Response response = get(baseUri);

        // ------------------------------ Then ------------------------------//
        List<SwimmingPoolDTO> responseBody = response.then()
                .log().ifValidationFails()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                .contentType(ContentType.JSON)
                .extract().body().jsonPath().getList(".", SwimmingPoolDTO.class);

        assertThat(responseBody).hasSize(1);
        SwimmingPoolDTO actualSwimmingPoolDTO = responseBody.get(0);
        
        assertThat(actualSwimmingPoolDTO).extracting(
                SwimmingPoolDTO::getId,
                SwimmingPoolDTO::getName)
                .containsExactly(
                        expectedSwimmingPool.getId(),
                        expectedSwimmingPool.getName());

        assertThat(actualSwimmingPoolDTO.getAddress()).isNotNull()
                .extracting(
                        AddressDTO::getCity,
                        AddressDTO::getPostalCode,
                        AddressDTO::getLine1)
                .containsExactly(
                        expectedSwimmingPool.getAddress().getCity(),
                        expectedSwimmingPool.getAddress().getPostalCode(),
                        expectedSwimmingPool.getAddress().getLine1());
    }
}
