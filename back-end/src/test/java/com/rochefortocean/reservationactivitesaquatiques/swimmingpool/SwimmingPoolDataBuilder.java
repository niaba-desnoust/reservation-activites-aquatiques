package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import static com.rochefortocean.reservationactivitesaquatiques.swimmingpool.AddressDataBuilder.anAddress;
import static com.rochefortocean.reservationactivitesaquatiques.swimmingpool.AddressDataBuilder.anAddressDTO;

import java.util.Collections;
import java.util.List;

public class SwimmingPoolDataBuilder {

    public static SwimmingPool.SwimmingPoolBuilder aSwimmingPool() {
        return SwimmingPool.aSwimmingPool()
                .withId(1L)
                .withName("Piscine Municipale Jean Langet")
                .withWebsite("https://www.ville-rochefort.fr/piscine-municipale-jean-langet")
                .withAddress(anAddress().build());
    }

    public static List<SwimmingPool> aSwimmingPoolList() {
        return Collections.singletonList(aSwimmingPool().build());
    }

    public static SwimmingPoolDTO.SwimmingPoolDTOBuilder aSwimmingPoolDTO() {
        return SwimmingPoolDTO.aSwimmingPoolDTO()
                .withId(1L)
                .withName("Piscine Municipale Jean Langet")
                .withWebsite("https://www.ville-rochefort.fr/piscine-municipale-jean-langet")
                .withAddress(anAddressDTO().build());
    }

    public static List<SwimmingPoolDTO> aSwimmingPoolDTOList() {
        return Collections.singletonList(aSwimmingPoolDTO().build());
    }
}
