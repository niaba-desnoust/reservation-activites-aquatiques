/* ************************************************************************************************************
 *
 * Réservation d'activités aquatiques
 *
 * Création de la table des piscines
 *
 *************************************************************************************************************
 *  Date        | Auteur                | Commentaire
 *  11/09/2021  | N. DESNOUST           | 
 *************************************************************************************************************/

---------------------------------------------------------------------------------------------------------------
-- Schémas
---------------------------------------------------------------------------------------------------------------

CREATE SCHEMA IF NOT EXISTS raa_schema;
set search_path to raa_schema;

---------------------------------------------------------------------------------------------------------------
-- Séquences
---------------------------------------------------------------------------------------------------------------

CREATE SEQUENCE raa_schema.swimming_pool_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

---------------------------------------------------------------------------------------------------------------
-- Tables
---------------------------------------------------------------------------------------------------------------

CREATE TABLE raa_schema.swimming_pool (
    id bigint NOT NULL DEFAULT nextval('raa_schema.swimming_pool_id_seq'),
    address_city varchar(255) NOT NULL,
    address_line1 varchar(255) NOT NULL,
    address_line2 varchar(255) NULL,
    address_postal_code varchar(5) NOT NULL,
    name varchar(255) NOT NULL,
    website varchar(500) NULL,
    CONSTRAINT swimming_pool_pkey PRIMARY KEY (id)
);
