package com.rochefortocean.reservationactivitesaquatiques;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReservationActivitesAquatiquesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReservationActivitesAquatiquesApplication.class, args);
	}

}
