package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import java.util.List;

import org.springframework.http.ResponseEntity;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Catalogue")
public interface SwimmingPoolControllerSpecification {

	@Operation( //
			summary = "Récuperer toutes les piscines", //
			description = "Actuellement, cette route ne supporte pas la pagination." //
	)
	ResponseEntity<List<SwimmingPoolDTO>> findAllSwimmingPools();

}
