package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@Builder(builderMethodName = "anAddress", setterPrefix = "with")
@NoArgsConstructor
@AllArgsConstructor
public class Address implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(length = 255, nullable = false)
	private String city;
	@Column(length = 5, nullable = false)
	private String postalCode;
	@Column(length = 255, nullable = false)
	private String line1;
	@Column(length = 255, nullable = true)
	private String line2;

}