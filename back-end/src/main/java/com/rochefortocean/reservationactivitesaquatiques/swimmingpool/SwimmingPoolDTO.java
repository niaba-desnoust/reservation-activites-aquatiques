package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(builderMethodName = "aSwimmingPoolDTO", setterPrefix = "with")
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "Piscine")
public class SwimmingPoolDTO {

	@Schema( //
			description = "L'identifiant unique de la piscine.", //
			example = "1", //
			required = true //
	)
	private Long id;

	@Schema( //
			description = "Le nom de la piscine.", //
			example = "Piscine municipale Jean Langet", //
			required = true //
	)
	private String name;

	@Schema( //
			description = "Le site web de la piscine.", //
			example = "https://www.ville-rochefort.fr/piscine-municipale-jean-langet", //
			required = false //
	)
	private String website;

	@Schema( //
			description = "L'adresse de la piscine.", //
			required = true //
	)
	private AddressDTO address;

}
