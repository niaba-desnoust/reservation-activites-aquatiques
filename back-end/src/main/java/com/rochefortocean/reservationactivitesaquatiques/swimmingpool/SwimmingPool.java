package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Builder(builderMethodName = "aSwimmingPool", setterPrefix = "with")
@NoArgsConstructor
@AllArgsConstructor
public class SwimmingPool implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(generator = "swimming_pool_gen")
	@SequenceGenerator(name = "swimming_pool_gen", sequenceName = "swimming_pool_id_seq", allocationSize = 1)
	private Long id;
	@Column(length = 255, nullable = false)
	private String name;
	@Column(length = 500, nullable = true)
	private String website;
	@Embedded
	private Address address;

}

