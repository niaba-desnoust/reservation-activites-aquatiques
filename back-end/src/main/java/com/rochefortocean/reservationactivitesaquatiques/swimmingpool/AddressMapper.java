package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper( //
		componentModel = "spring", //
		injectionStrategy = InjectionStrategy.CONSTRUCTOR, //
		builder = @Builder(disableBuilder = true) //
)
public interface AddressMapper {
	AddressDTO toDto(Address address);
}