package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder(builderMethodName = "anAddressDTO", setterPrefix = "with", toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Schema(name = "Adresse")
public class AddressDTO {
	@Schema( //
			description = "La ville dans laquelle la piscine se situe.", //
			example = "Rochefort", //
			required = true //
	)
	private String city;

	@Schema( //
			description = "Le code postal le plus proche de la piscine.", //
			example = "17300", //
			required = true //
	)
	private String postalCode;

	@Schema( //
			description = "Un champ qui contient les informations principales de l'adresse (numéro de rue, nom de rue, etc).", //
			example = "Rue Charles Maher", //
			required = true //
	)
	private String line1;

	@Schema( //
			description = "Un champ qui contient les informations secondaires de l'adresse (bâtiment, etc).", //
			example = "Bâtiment B", //
			required = false //
	)
	private String line2;
}
