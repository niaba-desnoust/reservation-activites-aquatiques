package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper( //
		componentModel = "spring", //
		uses = { AddressMapper.class }, //
		injectionStrategy = InjectionStrategy.CONSTRUCTOR, //
		builder = @Builder(disableBuilder = true) //
)
public interface SwimmingPoolMapper {
	SwimmingPoolDTO toDto(SwimmingPool swimmingPool);
}
