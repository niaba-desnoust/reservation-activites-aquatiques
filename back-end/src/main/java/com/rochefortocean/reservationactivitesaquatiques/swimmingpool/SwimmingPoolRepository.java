package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SwimmingPoolRepository extends JpaRepository<SwimmingPool, Long> {

}
