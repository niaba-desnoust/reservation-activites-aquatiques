package com.rochefortocean.reservationactivitesaquatiques.swimmingpool;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "/api/swimming-pools", produces = "application/json")
@AllArgsConstructor
public class SwimmingPoolController implements SwimmingPoolControllerSpecification {

	private final SwimmingPoolRepository swimmingPoolRepository;
	private final SwimmingPoolMapper swimmingPoolMapper;

	@GetMapping
	public ResponseEntity<List<SwimmingPoolDTO>> findAllSwimmingPools() {
		List<SwimmingPool> swimmingPools = swimmingPoolRepository.findAll();
		List<SwimmingPoolDTO> swimmingPoolsDTO = swimmingPools.stream()
				.map(swimmingPoolMapper::toDto)
				.collect(Collectors.toList());
				
		return ResponseEntity.ok(swimmingPoolsDTO);
	}

}
