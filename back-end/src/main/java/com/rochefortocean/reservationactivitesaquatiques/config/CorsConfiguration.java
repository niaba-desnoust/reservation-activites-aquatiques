package com.rochefortocean.reservationactivitesaquatiques.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
@EnableConfigurationProperties(CorsConfigurationProperties.class)
public class CorsConfiguration {

	@Bean
	public CorsFilter corsFilter(@Autowired CorsConfigurationProperties corsConfigurationProperties) {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		org.springframework.web.cors.CorsConfiguration config = new org.springframework.web.cors.CorsConfiguration();
		
		config.setAllowCredentials(true);
        corsConfigurationProperties.getAllowedOrigins().forEach(config::addAllowedOrigin);
        corsConfigurationProperties.getAllowedHeaders().forEach(config::addAllowedHeader);
        corsConfigurationProperties.getAllowedMethods().forEach(config::addAllowedMethod);
		source.registerCorsConfiguration("/**", config);
		
		return new CorsFilter(source);
	}

}