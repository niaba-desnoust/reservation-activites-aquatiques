# Décompose la variable d'environnement DATABASE_URL fournie 
# par Heroku en trois nouvelles variables :
#   - SPRING_DATASOURCE_URL
#   - SPRING_DATASOURCE_USERNAME
#   - SPRING_DATASOURCE_PASSWORD
# Ces variables sont directement interprétées par Spring Boot 
# pour configurer une datasource PostgreSQL.

echo "Splitting DATABASE_URL environment variable..."

DB_TYPE=$(echo $DATABASE_URL | cut -d':' -f1)"ql"
DB_TMP=$(echo $DATABASE_URL | cut -d':' -f2- | sed 's/^\/\///')
DB_TMP_USER_PASS=$(echo $DB_TMP | cut -d'@' -f1)

DB_HOST_PORT_DB=$(echo $DB_TMP | cut -d'@' -f2-)
DB_USER=$(echo $DB_TMP_USER_PASS | cut -d':' -f1)
DB_PASS=$(echo $DB_TMP_USER_PASS | cut -d':' -f2)

export SPRING_DATASOURCE_URL=$(echo "jdbc:$DB_TYPE://$DB_HOST_PORT_DB")
export SPRING_DATASOURCE_USERNAME=$DB_USER
export SPRING_DATASOURCE_PASSWORD=$DB_PASS

echo "DATABASE_URL successfully splitted."